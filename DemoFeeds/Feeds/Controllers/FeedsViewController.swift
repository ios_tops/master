//
//  FeedsViewController.swift
//  Demo
//
//  Created by Aiyub Munshi on 29/11/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

import SDWebImage
class FeedsViewController: UIViewController, PinterestLayoutDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {

    //MARK: - Outlets -
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionFeeds: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Variables -
    var itemsPosts : [(name : String, img : UIImage)] = [(name : "test1", img : #imageLiteral(resourceName: "home_img2_size_sample")),(name : "test2", img :#imageLiteral(resourceName: "home_img1_size_sample")),(name : "test3", img :#imageLiteral(resourceName: "home_img1_size_sample")),(name : "test4", img :#imageLiteral(resourceName: "home_img1_size_sample")),(name : "test5", img :#imageLiteral(resourceName: "home_img2_size_sample")),(name : "test6", img :#imageLiteral(resourceName: "home_img2_size_sample")),(name : "test7", img :#imageLiteral(resourceName: "home_img1_size_sample"))]
    var catItems : [(id : String, name : String)] = [(id : "1", name :"cat1"),(id : "2", name :"cat2"),(id : "3", name :"cat3"),(id : "4", name :"cat4"),(id : "5", name :"cat5"),(id : "6", name :"cat6"),(id : "7", name :"cat7")]
   var selectedIndexArr : [String] = []
    
    //MARK: - LifeCycle Methos -
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.configureView(comletino: {
            
            
            
            
            
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - View Configuration -
    func configureView(comletino: ()->())
    {
        collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
        if let layout = collectionFeeds?.collectionViewLayout as? PinterestLayout
        {
            layout.delegate = self
        }
        comletino()
        collectionFeeds.contentInset = UIEdgeInsetsMake(10, 10, 0, 10)
        
        
    }
    
    //MARK: - CollectionView Flowlayout Methods -
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return collectionView == self.collectionView ? CGSize(width: 66, height: 66) : CGSize(width: ((collectionView.frame.size.width-40)/3), height: ((collectionView.frame.size.width/3)))
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return collectionView == self.collectionView ? 10 : 0
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return collectionView == self.collectionView ? 10 : 0
    }
    
    //MARK: - CollectionView Delegate -
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        let image = itemsPosts[indexPath.row].img
        let ratio = image.size.width/image.size.height
        return (((collectionView.frame.size.width-60)/3)/ratio)<40 ? 40 : ((collectionView.frame.size.width-60)/3)/ratio
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.collectionView{
            
            if selectedIndexArr.contains(self.catItems[indexPath.row].id){
                self.selectedIndexArr.remove(at: self.selectedIndexArr.index(of: self.catItems[indexPath.row].id)!)
            }else{
                self.selectedIndexArr.append(self.catItems[indexPath.row].id)
            }
            
            self.collectionView.reloadData()
        }else{
            let vc = Utilities.viewController("FeedDetailViewController", onStoryboard: "Feeds") as! FeedDetailViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: - CollectionView Datasource -
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
            return collectionView == self.collectionView ? catItems.count : itemsPosts.count
    }
    internal func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.collectionView
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "catCell", for: indexPath) as! catCell
            cell.lblCat.text = self.catItems[indexPath.row].name
            cell.lblCat.adjustsFontSizeToFitWidth = true
            cell.lblCat.textAlignment = .center
            cell.outerView.layer.borderWidth=2
            cell.outerView.layer.borderColor = self.selectedIndexArr.contains(self.catItems[indexPath.row].id) ? themeOrangeColor.cgColor : UIColor.gray.cgColor
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCell", for: indexPath) as! FeedCell
            cell.imgView.image = self.itemsPosts[indexPath.row].img
            cell.lblPrice.text = "$200"
            return cell
        }
        
    }
    
    //MARK: - Button Actions -
    @IBAction func addTapped(_ sender: Any) {
        let vc = Utilities.viewController("AddNewFeedViewController", onStoryboard: "Feeds") as! AddNewFeedViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func filterTapped(_ sender: Any)
    {
        let vc = Utilities.viewController("FilterViewController", onStoryboard: "Feeds") as! FilterViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func menuTapped(_ sender: Any)
    {

    }

}
class FeedCell : UICollectionViewCell{
    @IBOutlet weak var lblPrice : UILabel!
    @IBOutlet weak var imgView : UIImageView!
}

class catCell : UICollectionViewCell
{
    @IBOutlet weak var outerView : UIView!
    @IBOutlet weak var lblCat : UILabel!
}
