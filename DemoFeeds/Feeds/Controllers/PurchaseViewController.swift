//
//  PurchaseViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 04/12/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit
import DropDown


class PurchaseViewController: UIViewController
{
    //MARK: - Outlets -
    @IBOutlet weak var btnSelectDelMet: UIButton!
    @IBOutlet weak var btnSelectPM: UIButton!
    @IBOutlet weak var btnSelectState: UIButton!
    @IBOutlet weak var lblleftAdress: UILabel!
    
    //MARK: - Variables -
    var selectDeliveryMethod = DropDown()
    var selectState = DropDown()
    var selectPaymentMethod = DropDown()
    
    //MARK: - Lifecycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDD()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Dropdowns setup -
    func setupDD()
    {
        selectDeliveryMethod.anchorView = self.btnSelectDelMet
        selectDeliveryMethod.dataSource = ["Shipping", "Local Meetup"]
        selectDeliveryMethod.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectDelMet.setTitle(item, for: .normal)
            self.lblleftAdress.text = index == 1 ? "Meetup address:" : "Shipping address:"
        }
        selectState.anchorView = self.btnSelectState
        selectState.dataSource = ["Arizona", "California"]
        selectState.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectState.setTitle(item, for: .normal)
        }
        selectPaymentMethod.anchorView = self.btnSelectPM
        selectPaymentMethod.dataSource = ["Card", "Cash on delivery", "Credit card"]
        selectPaymentMethod.selectionAction = { [unowned self] (index: Int, item: String) in
            self.btnSelectPM.setTitle(item, for: .normal)
        }
    }

    //MARK: -  Button Actions -
    @IBAction func purchaseTapped(_ sender: Any)
    {
       let vc = Utilities.viewController("OrderConfirmationViewController", onStoryboard: "Feeds") as! OrderConfirmationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func selectDeviveryMethodTapped(_ sender: Any)
    {
        selectDeliveryMethod.show()
    }
    @IBAction func selectCardTapped(_ sender: Any)
    {
        selectPaymentMethod.show()
    }
    @IBAction func selectStateTapped(_ sender: Any) {
        selectState.show()
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
