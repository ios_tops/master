

import Foundation

// Define your api actions
enum APIAction: String
{
    case userRegister = "userRegister"
    case editProfile = "editProfile"
    case loginWithGoogle = "loginWithGoogle"
    case getProductDetails = "getProductDetails"
    case getUserStore = "getUserStore"
    case changePassword = "changePassword"
    case login = "login"
    case forgotPassword = "forgotPassword"
    case registerWithGoogle = "registerWithGoogle"
    case registerWithFacebook = "registerWithFacebook"
    case loginWithFacebook = "loginWithFacebook"
    case getUserProfile = "getUserProfile"
    case followSeller = "followSeller"
    case publishReview = "publishReview"
    case makeOffer = "makeOffer"
    case getCategories = "getCategories"
    case getSubCategories = "getSubCategories"
    case getProducts = "getProducts"
    case publishProduct = "publishProduct"
    case addToWishlist = "addToWishlist"
    case deleteFromWishlist = "deleteFromWishlist"
    case getWishList = "getWishList"
    case getUserReview = "getUserReview"
    case reportProduct = "reportProduct"
    case resendEmail = "verifyEmail"
    case getDashboardDetail = "getDashboardDetail"
    case getThreads = "getThreads"
    case getMessage = "getMessage"
    case sendMessage = "sendMessage"
}

