import UIKit
import JDStatusBarNotification

/***** API Credentials *****/

private let liveURL : String =  ""
private let localURL : String = ""

private let isLive : Bool = false   // Api Environment, "Live = true, Local = false"

// Base Api URL
var baseURL: String
{
    get
    {
        return (isLive) ? liveURL : localURL
    }
}

let encryptionKey : String = "}{(**&%%^$%@#$&*!#$%#&*^&*"
private let _sharedNetworkController = NetworkController()
let kNetworkController = NetworkController.sharedInstance
class NetworkController: NSObject {
    
    let reachability: Reachability = Reachability.forInternetConnection()
    var hasInternetConnection: Bool = false
    var forgotpassword:Bool = false
    // MARK: Singleton
    /* init */
    override init() {
        super.init()
        /* <#comment#> */
        startUp()
    }
    
    /* shared instance */
    class var sharedInstance: NetworkController {
        return _sharedNetworkController
    }
    
    func checkNetworkStatus() -> Bool {
        let networkStatus: NetworkStatus = reachability.currentReachabilityStatus()
        hasInternetConnection = (networkStatus != NetworkStatus.NotReachable)
        if !hasInternetConnection {
            UIApplication.shared.endIgnoringInteractionEvents()
            JDStatusBarNotification.show(withStatus: "it seems you are offline...", dismissAfter: 2.0, styleName: JDStatusBarStyleError)
//            Utilities.showToastWithErrorMessage(Physician.getLocaliseString("it seems you are offline..."))
            
            //Utilities.alertforconnection()
            /*let resultVC = Utilities.viewController("NetworkViewController", onStoryboard: "Intro") as! NetworkViewController
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(resultVC, animated: true, completion: nil)*/
           
            return false
        }
        return true
    }
    
    // MARK: Handle network status
    @objc func networkStatusDidChange(notification: NSNotification?) {
        let networkStatus: NetworkStatus = reachability.currentReachabilityStatus()
        hasInternetConnection = (networkStatus != NetworkStatus.NotReachable)
        
        if !hasInternetConnection {
            JDStatusBarNotification.show(withStatus: "it seems you are offline...", dismissAfter: 2.0, styleName: JDStatusBarStyleError)
//Utilities.showToastWithErrorMessage(Physician.getLocaliseString("it seems you are offline..."))
        
           /* let resultVC = Utilities.viewController("NetworkViewController", onStoryboard: "Intro") as! NetworkViewController
            UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(resultVC, animated: true, completion: nil)*/
            
        }
        else{
            /*let resultVC = Utilities.viewController("NetworkViewController", onStoryboard: "Intro") as! NetworkViewController
            UIApplication.sharedApplication().keyWindow?.rootViewController?.dismissViewControllerAnimated(true, completion: nil)       */
            //Utilities.showToastWithSuccessMessage(Physician.getLocaliseString("INTERNET_CONNECTION_ON"))
            
        }
    }
    
    // MARK: Functions
    
    /* call this once at init */
    func startUp() {
        NotificationCenter.default.addObserver(self, selector: #selector(NetworkController.networkStatusDidChange), name: NSNotification.Name.reachabilityChanged, object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: "networkStatusDidChange:", name: NSNotification.Name.reachabilityChanged, object: nil)
        reachability.startNotifier()
        //        networkStatusDidChange(nil)
    }
}
