//
//  AddPhotoViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 30/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import JDStatusBarNotification

class AddPhotoViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    //MARK : - Outlets -
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Variables -
    var imagePicker = UIImagePickerController()
    var items : [(image : UIImage?, key : String)] = []
    var productTitle : String = ""
    var sellingPrize : String = ""
    var isNagotiable : Bool = false
    var catId : String = ""
    var subCatId : String = ""
    var productDisc : String = ""
    var producLoc : CLLocationCoordinate2D = CLLocationCoordinate2D()
    var productAddress : String = ""
    
    //MARK: - Lifecycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - ImagePickerController Delegate -
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if (info[UIImagePickerControllerOriginalImage] as? UIImage) != nil {
            self.items.append((image: (info[UIImagePickerControllerEditedImage] as? UIImage)!, key: "productImage"))
            self.collectionView.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Show Camera -
    func showCam()
    {
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Photo Gallary", style: .default)
        { _ in
            
            if UIImagePickerController .isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .photoLibrary
                self .present(self.imagePicker, animated: true, completion: nil)
                
            } else {
                let NocamAvailalble = "PhotoGallery not available in your device!"
                self.showAlert(title: appName, message: NocamAvailalble)
            }
            
            
        }
        actionSheetController.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            
            if UIImagePickerController .isSourceTypeAvailable(.camera) {
                self.imagePicker.allowsEditing = true
                self.imagePicker.sourceType = .camera
                self .present(self.imagePicker, animated: true, completion: nil)
            } else {
                let NocamAvailalble = "Camera not available in your device!"
                self.showAlert(title: appName, message: NocamAvailalble)
            }
            
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    //MARK: - Button Actions -
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func nextTapped(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    //MARK: - Publish Product API Call Sample-
    func publishProduct()
    {
        let params = ["sellerId":"1",
                      "title" : self.productTitle,
                      "name" : self.productTitle,
                      "description" : self.productDisc,
                      "price" : self.sellingPrize,
                      "lat" : self.producLoc.latitude.description,
                      "long" : self.producLoc.longitude.description,
                      "isNagotiable" : self.isNagotiable ? "yes" : "no",
                      "category" : self.catId,
                      "subcateory" : self.subCatId,
                      "address" : self.productAddress]
        AlamofireModel.alamofireMethod(methods: .post, url: APIAction.publishProduct.rawValue, parameters: params, Header: [:], handler: { (response) in
            JDStatusBarNotification.show(withStatus: "Product Published Successfully.", dismissAfter: 3.0, styleName: JDStatusBarStyleError)
            self.navigationController?.popToRootViewController(animated: true)
        }) { (error) in
            JDStatusBarNotification.show(withStatus: error.localizedDescription, dismissAfter: 3.0, styleName: JDStatusBarStyleError)
        }
    }
}
extension AddPhotoViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    //MARK: - CollectionView Datasource/Delegate & Flow layout-
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        if indexPath.row == 0 {
            cell.defaultImage.isHidden = false
            cell.imgView.image = nil
        }else{
            cell.defaultImage.isHidden = true
            cell.imgView.isUserInteractionEnabled = true
            cell.imgView.image = items[indexPath.row-1].image
//            cell.imgView.setupForImageViewer()
            cell.imgView.contentMode = .scaleAspectFill
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.size.width/2)-10, height: (self.view.frame.size.width/2)-10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if indexPath.row == 0
            {
                showCam()
            }
    }
}
class PhotoCell : UICollectionViewCell{
    @IBOutlet weak var defaultImage: UIImageView!
    @IBOutlet weak var imgView: UIImageView!
}
extension UIViewController
{
    //MARK: - Alert displaying setups sample -
    func showAlert(title:String ,message:String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertwithHandler(title:String ,message:String, okAction : @escaping ()->()) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default) { (alert) in
            okAction()
        }
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showAlertwithHandlercancel(title:String ,message:String, okAction : @escaping ()->()) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default) { (alert) in
            okAction()
        }
        let cancelAction1 = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(cancelAction1)
        self.present(alert, animated: true, completion: nil)
    }
}

