//
//  Utilities.swift
//  Bityo
//
//  Created by Aiyub Munshi on 29/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import Foundation
import UIKit
class Utilities
{
    class func viewController(_ name: String, onStoryboard storyboardName: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
}
