//
//  Product.swift
//  Bityo
//
//  Created by Aiyub Munshi on 10/01/18.
//  Copyright © 2018 Aiyub Munshi. All rights reserved.
//

import Foundation
import SwiftyJSON

class Product
{
    var productId = ""
    var productTitle = ""
    var productName = ""
    var price = 0
    var showImage = ""
    var address = ""
    var images : [(String)] = []

    init() {}
    
    convenience init(productInfo : JSON){
        self.init()
        if let x = productInfo["productId"].string{
            productId = x
        }
        if let x = productInfo["productTitle"].string{
            productTitle = x
        }
        if let x = productInfo["productName"].string{
            productName = x
        }
        if let x = productInfo["price"].int{
            price = x
        }
        if let x = productInfo["image"].array{
            self.showImage = x[0].stringValue
            for xx in x{
                self.images.append(xx.stringValue)
            }
        }
        if let x = productInfo["address"].string{
            self.address = x
        }
    }
}
