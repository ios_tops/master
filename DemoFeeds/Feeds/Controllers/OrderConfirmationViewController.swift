//
//  OrderConfirmationViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 04/12/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit

class OrderConfirmationViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var viewPurchaseSuc: UIView!
    @IBOutlet weak var viewOption: UIView!
    
    //MARK : LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Hide/Show Success alertbox -
    func showSuccessAnimated(){
        self.viewOption.isHidden = false
        self.viewPurchaseSuc.frame.origin.y = self.view.frame.height
        UIView.animate(withDuration: 0.5, animations: {
            self.viewPurchaseSuc.center = self.view.center
        }) { (success) in
        }
    }
    func hideSuccessAnimated(){
        self.viewPurchaseSuc.center = self.view.center
        UIView.animate(withDuration: 0.5, animations: {
            self.viewPurchaseSuc.frame.origin.y = self.view.frame.height
        }) { (success) in
                self.viewOption.isHidden = true
                self.navigationController?.popToRootViewController(animated: false)
            }
        }
    

    //MARK: - Button Actions -
    @IBAction func continueTapped(_ sender: Any) {
        self.hideSuccessAnimated()
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func doneTapped(_ sender: Any) {
        self.showSuccessAnimated()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
