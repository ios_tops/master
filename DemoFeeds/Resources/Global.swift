//
//  Global.swift
//  Bityo
//
//  Created by Aiyub Munshi on 29/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import Foundation


//MARK: - Color -

let themeOrangeColor = rgbaToUIColor(red: 255/255, green: 203/255, blue: 33/255, alpha: 1)
let themeLightGrayColor = rgbaToUIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
let appName = "Bityo"

//MARK: - Social IDs and Secrets -

let facebookAppId = "2021647104747581"
let facebookSecret = "f5b89600ec9b30d16c4f2c2f896b9326"


let default_failure = "Something went wrong."

public func rgbaToUIColor(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
    
    return UIColor(red: red, green: green, blue: blue, alpha: alpha)
}
class CustomSlide: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 2
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(origin: bounds.origin, size: CGSize(width : bounds.width, height : trackHeight))
    }
}
extension String {
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
    }
    
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    
    var uppercaseFirst: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    
    
}
