— FEEDS DEMO (MVC Structured Architecture)
 
This demo is having static details for the feeds(Posted Item for sale) listing. It is also having details and filter screen which is not functionally working but we can see and utilize the structure tree and the code.
 
Implementation of MVC :
 
 - Feeds is the feature of the App so Model, Views, and controllers will be listed inside the each and every feature. Each and every group will contain the related files.
 I.E. All the controllers related to the feed features are maintained inside the Features -> Feeds -> Controllers
  
 - Same will be applied for the View and Model. You can check for the Product Model inside the project.
  
 - We have implemented nonfiction things for best UI. But you can check the resources folder which is also having support files for the API calls.
  
 - There are classes like AlamofireModel and NetworkManager. Which used to get and handle the response effectively and efficiently.
  
 - Almofire is used to call API and get the response which is handled by SwiftyJSON for proper and clean mapping the keys with the data.
  
 - With using this approach We will have the model filled without nil issues that causes crash into the app. That model will be used for filling the views.
  
 Other Configurations :
  
 - Configurations like Loader and required notification appearances is simply done by drag and drop the Library code into the  Resources group and some of them are added via pods, too.
  
 - There is some Global and Utilities class that you can check inside the Resources group. Global contains the global variables and methods which is used across the whole project.
  
 - For the network management, We are using Network manager Class for handling the network changes and the activities.
  
 Layouts and UI :
 - FeedsViewController.swift is having feeds UI which can be filtered by category, search and also customized by choice (Filter). This feed list is having the customized layout like Pinterest.
  
      - FeedsDetailsViewController.swift is containing complete details of the particular feed. Therefore, this screen must have the lazy layout with scrolling as per the content. So, It is managed accordingly.
       
      - Adding Feeds(Item for sale) is also implemented nicely with the validations of the Email, number rule, and animations. Here, we are using SwiftValidator() To validate the fields easily.
       
       
      We are uploading this sample as a considerable MVC Structured demo of one single module and simple structure of model and API calling with required class.
       

