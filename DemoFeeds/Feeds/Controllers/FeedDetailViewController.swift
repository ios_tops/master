//
//  FeedDetailViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 29/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit
import GoogleMaps
import Social
import SwiftyJSON
import JDStatusBarNotification

class FeedDetailViewController: UIViewController {

    //MARK: - Outlets -
    @IBOutlet weak var lblProductPriceSecond: UILabel!
    @IBOutlet weak var lblBumpingPrice: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var txtOffer: UITextField!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnPopUpDown: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var viewBumpingInfo: UIView!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var viewSeller: UIView!
    @IBOutlet weak var constSellerInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var popUpLeading: NSLayoutConstraint!
    @IBOutlet weak var popupYConst: NSLayoutConstraint!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnFav: UIButton!
    @IBOutlet weak var btnFollow: UIButton!
    @IBOutlet weak var lblReview: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var viewRating: FloatRatingView!
    @IBOutlet weak var imgSeller: UIImageView!
    @IBOutlet weak var lblSellerName: UILabel!
    @IBOutlet weak var lblPrizeBC: UILabel!
    @IBOutlet weak var lblNagotiable: UILabel!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var lblSubCat: UILabel!
    @IBOutlet weak var lblCat: UILabel!
    @IBOutlet weak var lblProductName2: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var mainHeight: NSLayoutConstraint!
    @IBOutlet weak var followHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNagotiable2: UILabel!
    
    //MARK: - Variables -
    var isFrom = "feed"
    var currentProduct : Product?
    var imagesSlider : [String] = []
    var sellerId = ""
    var isFollowedByUser = false
    let validator = Validator()
    var sellerName = ""
    var imageToBeShared = #imageLiteral(resourceName: "placeHolderProduct")
    
    //MARK: - Lifecycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - View Configuration -
    func configureView(){
        btnLeft.setTitle("Make offer", for: .normal)
        btnRight.setTitle("Purchase", for: .normal)
        self.viewBumpingInfo.isHidden = true
        btnPopUpDown.setTitle("Send offer", for: .normal)
        self.validator.registerField(textField: self.txtOffer, rules: [RequiredRule(),FloatRule()])
        DispatchQueue.main.async {
            self.imgSeller.layer.cornerRadius = 51
        }
        self.popUpLeading.constant = self.view.frame.size.width + 20
        self.popupYConst.constant = -420
        self.optionView.isHidden = true
    }
    
    //MARK: - Pop up hide / unhide -
    func showPopUp(){
        self.optionView.isHidden = false
        self.popupYConst.constant = 30
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            
        }
    }
    func hidePopup(){
        self.popupYConst.constant = -410
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            self.optionView.isHidden = true
        }
    }
    
    func showSharePopUp(){
        self.optionView.isHidden = false
        self.popUpLeading.constant = 10
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            
        }
    }
    func hideSharePopup()
    {
        self.popUpLeading.constant = self.view.frame.size.width + 20
        self.popupYConst.constant = -410
        UIView.animate(withDuration: 0.4, animations:
            {
                self.view.layoutIfNeeded()
        }) { (success) in
            self.optionView.isHidden = true
        }
    }
    
    //MARK: - Map Marker setup -
    func setMarker(location : CLLocationCoordinate2D){
        let marker = GMSMarker()
        marker.position = location
        marker.icon = #imageLiteral(resourceName: "map_pin")
        marker.map = self.mapView
        self.mapView.animate(toLocation: location)
    }
    
    //MARK: - Button Actions -
    @IBAction func followTapped(_ sender: Any) {
        //        followSeller()
    }
    @IBAction func btnReportTapped(_ sender: Any) {
        //        reportProduct()
    }
    @IBAction func backGrouundTapped(_ sender: Any)
    {
        self.view.endEditing(true)
        hideSharePopup()
    }
    @IBAction func shareProductTapped(_ sender: Any)
    {
        showSharePopUp()
    }
    @IBAction func sendOfferTapped(_ sender: Any)
    {
    }
    @IBAction func closeTapped(_ sender: Any)
    {
        self.view.endEditing(true)
        hidePopup()
    }
    @IBAction func profileTapped(_ sender: Any) {

    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func changePage(_ sender: UIPageControl)
    {
    }
    @IBAction func starTapped(_ sender: UIButton) {
         sender.isSelected = !sender.isSelected
    }
    @IBAction func leftTapped(_ sender: Any)
    {
            self.showPopUp()
    }
    @IBAction func shareLinkedIn(){
       
    }
    @IBAction func shareTwitter(){
      
    }
    @IBAction func shareFBTapped(_ sender: Any) {
      
    }
    @IBAction func rightTapped(_ sender: Any) {
            let vc = Utilities.viewController("PurchaseViewController", onStoryboard: "Feeds") as! PurchaseViewController
            self.navigationController?.pushViewController(vc, animated: true)
    }
}


