    
//

import UIKit
import Alamofire
import Foundation
import SwiftyJSON
import JDStatusBarNotification

var hide_loader_checkReminder = false

class AlamofireModel: NSObject
{
    typealias apiSuccess = (_ result: Any?) -> Void
    typealias apiFailure = (_ error: Any?) -> Void
    

    class func verifyResponseData(data: JSON?) -> (result: JSON?, error: String?) {
        
        var verifyResult: (result: JSON?, error: String?) = (data, nil)
        
        let status = data?["status"]
        if (status?.string == "Success") {
            // success
        } else {
            let message = data?["message"]
            verifyResult.result = nil
            verifyResult.error = message?.string
        }
        return verifyResult
    }

    
    typealias CompletionHandler = (_ response:AnyObject) -> Void
    typealias ErrorHandler = (_ error : NSError) -> Void
    
    
    class func alamofireMethod(methods: Alamofire.HTTPMethod , url : URLConvertible , parameters : [String : Any],Header: [String: String],handler:@escaping CompletionHandler,errorhandler : @escaping ErrorHandler)
    {
        
       
        if NetworkController.sharedInstance.checkNetworkStatus()
        {
            var alamofireManager : Alamofire.SessionManager?
            var hed = Header
            //hed["Connection"] = "close"
            var UrlFinal = ""
            do
            {
               try UrlFinal = baseURL + url.asURL().absoluteString
            }
            catch{}
            print(UrlFinal)
            
            if !hide_loader_checkReminder
            {
//                DELEGATE.showLoader()
            }else
            {
                hide_loader_checkReminder = false
            }

            
            
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForResource = 25
            configuration.timeoutIntervalForRequest = 25
            configuration.httpAdditionalHeaders  = hed
            alamofireManager = Alamofire.SessionManager(configuration: configuration)
            
            alamofireManager = Alamofire.SessionManager.default
            
            alamofireManager?.request(UrlFinal, method: methods, parameters: parameters, headers: [:]).responseJSON(queue: nil, options: JSONSerialization.ReadingOptions.allowFragments, completionHandler: { (response) in
                print(parameters)
                print(UrlFinal)
                print(hed)
//                DELEGATE.hideLoader()
                
                if response.result.isSuccess
                {
                    print(response)
                    let res = JSON(response.result.value as Any)
                    if let statuscode = res["statusCode"].int
                    {
                        if statuscode == 200 || statuscode == 408
                        {
                            handler(response.result.value! as AnyObject)
                        }
                        else
                        {
                                JDStatusBarNotification.show(withStatus: res["Message"].stringValue, dismissAfter: 3.0, styleName: JDStatusBarStyleError)
                        }
                    }
                }
                else
                {
                    JDStatusBarNotification.show(withStatus: default_failure, dismissAfter: 3.0, styleName: JDStatusBarStyleError)
                    errorhandler(response.result.error! as NSError)
                }
            })
        }
    }
    
    class func uploadmultipleImage(_ strURL: String, parameters: [String: String],imagewithkeys: [(image : UIImage?, key : String)], success:@escaping apiSuccess, failure:@escaping apiFailure)
    {
        if NetworkController.sharedInstance.checkNetworkStatus()
        {
//            DELEGATE.showLoader()
            print(parameters as Any)
            var UrlFinal = ""
            do
            {
                try UrlFinal = baseURL + strURL.asURL().absoluteString
            }
            catch
            {
                
            }
            print(UrlFinal)
            Alamofire.upload(
                multipartFormData: { MultipartFormData in
                    for (key, value) in (parameters)
                    {
                        MultipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
                    }
                    for (index,imageWithKey) in imagewithkeys.enumerated()
                    {
                        if imageWithKey.image != nil
                        {
                            MultipartFormData.append(UIImageJPEGRepresentation(imageWithKey.image!, 1)!, withName: imageWithKey.key, fileName: Date().timeIntervalSince1970.description+index.description, mimeType: "image/jpeg")
                        }
                        
                    }
            }, to: UrlFinal) { (result) in
                switch result
                {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseJSON { response in
                        print(response.result.value!)
//                        DELEGATE.hideLoader()
                        success(response.result.value)
                    }
                case .failure(let encodingError):
                    print(encodingError)
//                    DELEGATE.hideLoader()
                    failure(encodingError)
                }
            }

        }
    }
    
    class func uploadImage(_ strURL: String, parameters: [String: Any]?,image: UIImage?,keyImage : String, success:@escaping apiSuccess, failure:@escaping apiFailure) {
//        DELEGATE.showLoader()
        print(parameters as Any)
        var UrlFinal = ""
        do
        {
            try UrlFinal = baseURL + strURL.asURL().absoluteString
        }
        catch{
            
        }
        print(UrlFinal)
        Alamofire.upload(
            multipartFormData: { MultipartFormData in
                //    multipartFormData.append(imageData, withName: "user", fileName: "user.jpg", mimeType: "image/jpeg")
                
                for (key, value) in (parameters)! {
                    print((key, value))
                    MultipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
                }
                if image != nil{
                    MultipartFormData.append(UIImageJPEGRepresentation(image!, 1)!, withName: keyImage, fileName: "file1.jpeg", mimeType: "image/jpeg")
                }
                
        }, to: UrlFinal) { (result) in
            
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                    
                })
                
                upload.responseJSON { response in
                    if let res = response.result.value{
                        print(res)
//                        DELEGATE.hideLoader()
                        success(res)
                    }else{
                        JDStatusBarNotification.show(withStatus: default_failure, dismissAfter: 3.0, styleName: JDStatusBarStyleError)
//                        DELEGATE.hideLoader()
                    }
                    
                }
                
            case .failure(let encodingError):
                print(encodingError)
//                DELEGATE.hideLoader()
                failure(encodingError)
            }
        }
    }
    
    

}
