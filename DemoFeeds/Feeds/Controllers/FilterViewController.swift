//
//  FilterViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 29/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit
import WARangeSlider
import GooglePlaces
import GooglePlacePicker

class FilterViewController: UIViewController, GMSPlacePickerViewControllerDelegate {

    //MARK: - Outlets -
    @IBOutlet weak var lblSelectedLocation: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var rangeSlider: UIView!
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var distanceSlider: CustomSlide!
    
    //MARK: - Variables -
    var rangeSliderr : RangeSlider!
    var toPrize = ""
    var forPrize = ""
    var filterDistanse = ""
    var preLocation = "Current location"    
    var maximumValue : Double = 100000
    var minimunValue : Double = 0
    var customLocationAvail = false
    var selectedLocation = CLLocationCoordinate2D()
    
    //MARK: - Lifecycle methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - View Configuration -
    func configureView()
    {
        DispatchQueue.main.async {
            self.setupRangeSlider()
        }
    }
    
    //MARK: - Slider setup -
    func setupRangeSlider(){
        rangeSliderr = RangeSlider(frame: rangeSlider.bounds)
        self.rangeSliderr.maximumValue = maximumValue
        self.rangeSliderr.minimumValue = minimunValue
        self.rangeSliderr.lowerValue = Double(100)
        self.rangeSliderr.upperValue = Double(100000)
        self.rangeSliderr.tintColor = themeOrangeColor
        self.rangeSliderr.trackHighlightTintColor = themeOrangeColor
        self.rangeSlider.addSubview(rangeSliderr)
        self.lblSelectedLocation.text = "Unknown Location"
        rangeSliderr.addTarget(self, action: #selector(self.rangeSliderValueChanged(sender:)), for: .valueChanged)
    }
    @objc func rangeSliderValueChanged(sender : RangeSlider)
    {
        lblFrom.text = "From: \(String(format: "%.2f", sender.lowerValue))"
        lblTo.text = "To: \(String(format: "%.2f", sender.upperValue))"
    }
    
    //MARK: - PlacePicker Delegate -
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: {
            self.customLocationAvail = true
            self.selectedLocation = place.coordinate
            if place.formattedAddress != nil {
                
                self.lblSelectedLocation.text = place.formattedAddress
            }else{
                self.lblSelectedLocation.text = "Unknown Location"
            }
            
        })
        
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        viewController.dismiss(animated: true, completion: {
            print(error.localizedDescription)
        })
        
    }
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //Mark : Button Actions
    @IBAction func applyTapped(_ sender: Any) {
    
        self.performSegue(withIdentifier: "unwindToFeeds", sender: self)
    }
    @IBAction func clearTapped(_ sender: Any) {
        self.selectedLocation = CLLocationCoordinate2D()
        self.lblSelectedLocation.text = "Current location"
        self.rangeSliderr.maximumValue = maximumValue
        self.rangeSliderr.minimumValue = minimunValue
        self.rangeSliderr.lowerValue = minimunValue
        self.rangeSliderr.upperValue = maximumValue
        self.distanceSlider.value = 100
    }
    @IBAction func changeValue(_ sender: UISlider) {
        self.lblMiles.text = "\(String(format: "%.2f", sender.value)) Km."
        
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func selectLocationTapped(_ sender: Any) {
        
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
}
