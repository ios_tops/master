
//
//  AddNewFeedViewController.swift
//  Bityo
//
//  Created by Aiyub Munshi on 30/11/17.
//  Copyright © 2017 Aiyub Munshi. All rights reserved.
//

import UIKit
import JDStatusBarNotification
import SwiftyJSON
import GooglePlaces
import GooglePlacePicker

class AddNewFeedViewController: UIViewController,GMSPlacePickerViewControllerDelegate {

    //MARK: - Outlets -
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtSellingPrice: UITextField!
    @IBOutlet weak var btnDown: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var yconstant: NSLayoutConstraint!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnNagotiable: UIButton!
    @IBOutlet weak var txtCat: UITextField!
    @IBOutlet weak var txtSubCategory: UITextField!
    @IBOutlet weak var lblSelectedLocation: UILabel!
    @IBOutlet weak var txtProductdes: KMPlaceholderTextView!
    
    //MARK: - Variables -
    var cat = 0
    var isFrom = "feed"
    var selectedLocation = CLLocationCoordinate2D()
    var categories = ["Category 1","Category 2","Category 3","Category 4","Category 5","Category 6","Category 7"]
    var subCategories = ["Sub Category 1","Sub Category 2","Sub Category 3","Sub Category 4","Sub Category 5","Sub Category 6","Sub Category 7"]
    let validator = Validator()
    
    //MARK: - Lifecycle Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - View Configuration -
    func configureView(){
        self.validator.registerField(textField: txtTitle, rules: [RequiredRule()])
        self.validator.registerField(textField: txtSellingPrice, rules: [RequiredRule(), FloatRule()])
        self.txtTitle.tag = 1001
        self.txtSellingPrice.tag = 1002
        self.optionView.isHidden = true
        self.yconstant.constant = self.view.frame.size.height
    }
    
    //MARK: - Placepicker delegate -
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: {
            
            self.selectedLocation = place.coordinate
            if place.formattedAddress != nil {
                
                self.lblSelectedLocation.text = place.formattedAddress
            }else{
                self.lblSelectedLocation.text = "Unknown Location"
            }
        })
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        viewController.dismiss(animated: true, completion: {
            print(error.localizedDescription)
        })
    }
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Validation -
    func validateAddProductDetails() -> Bool{
        if txtProductdes.text.trim() == ""{
            JDStatusBarNotification.show(withStatus: "Please enter description.", dismissAfter: 3.0, styleName: JDStatusBarStyleError)
            return false
        }
        return true
    }
   
    //MARK: - Hide/Unhide Views -
    func showPopUp(){
        self.optionView.isHidden = false
        self.yconstant.constant = 30
        
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            
        }
    }
    func hidePopup(){
        self.yconstant.constant = self.view.frame.height + 30
        UIView.animate(withDuration: 0.4, animations: {
            self.view.layoutIfNeeded()
        }) { (success) in
            self.optionView.isHidden = true
        }
    }

    //MARK: - Button Actions -
    @IBAction func selectLocationTapped(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    @IBAction func nagotialteTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        sender.isSelected = !sender.isSelected
    }
    @IBAction func deleteTapped(_ sender: Any) {
    }
    @IBAction func showSubCategoryTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.cat = 1
        self.tblView.reloadData()
        self.showPopUp()
    }
    @IBAction func showCategoryTapped(_ sender: Any) {
        self.view.endEditing(true)
        self.cat = 0
        self.tblView.reloadData()
        self.showPopUp()
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextTapped(_ sender: Any) {
        
        self.view.endEditing(true)
        var cnt = 2000
        var error : String = ""
        self.validator.validate { (errors) in
            for (textField, vError) in errors
            {
                if textField.tag < cnt
                {
                    cnt = textField.tag
                    let errorDescription = vError.errorMessage.replacingOccurrences(of:"{INPUT_FIELD}", with: (textField.placeholder!).lowercased())
                    error = errorDescription.uppercaseFirst
                }
            }
            if errors.count > 0
            {
                JDStatusBarNotification.show(withStatus: error, dismissAfter: 3.0, styleName: JDStatusBarStyleError)
            }
            else
            {
                if validateAddProductDetails() {
                    if isFrom == "edit" {
                        
                        //Call Update Profile API
                        self.navigationController?.popToRootViewController(animated: true)
                    }else{
                        let vc = Utilities.viewController("AddPhotoViewController", onStoryboard: "Feeds") as! AddPhotoViewController
                        vc.productTitle = self.txtTitle.text!
                        vc.sellingPrize = self.txtSellingPrice.text!
                        vc.producLoc = self.selectedLocation
                        vc.productAddress = self.lblSelectedLocation.text! == "Current Location" ? "" : self.lblSelectedLocation.text!
                        vc.isNagotiable = self.btnNagotiable.isSelected
                        vc.productDisc = self.txtProductdes.text!
                        //send all the details to add photo and do the add there
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
            }
        }
    }
    @IBAction func closeTapped(_ sender: Any) {
        hidePopup()
    }
    
   

}
extension AddNewFeedViewController : UITableViewDataSource, UITableViewDelegate{
    
    //MARK: - TableView Datasource/ Delegate  -
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.cat == 0 {
            return categories.count
        }
        else
        {
            return subCategories.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! categoryCell
        if self.cat == 0
        {
            cell.lblName.text = self.categories[indexPath.row]
        }
        else
        {
            cell.lblName.text = self.subCategories[indexPath.row]
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.cat == 0 {
            self.txtCat.text = self.categories[indexPath.row]
        }else{
            self.txtSubCategory.text = self.subCategories[indexPath.row]
        }
        self.hidePopup()
        
    }
}
class categoryCell : UITableViewCell{
    @IBOutlet weak var lblName : UILabel!
}
